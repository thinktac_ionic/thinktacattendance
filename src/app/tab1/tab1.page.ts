import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController, Platform, LoadingController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { UserProfileService } from '../services/user-profile.service';
import { firestore } from 'firebase';



@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
 
  loaderToShow: any;
  locationCoords: any;
  timetest: any;
  date:string;
  checkMessage: string = "Check Out";
  locationAddress: any
  checkedInDuration:number
  nowTime:number
  active:boolean;

  today = new Date();

  todayDocId: string = ""
  emailId: string = 'manzoor@thinktac.com'
  employeesCollection: string = 'Employees'
  attendanceCollection: string = 'Attendance';

  constructor(public router: Router,
    private nativeGeocoder: NativeGeocoder,
    private afAuth: AngularFireAuth,
    public loadingController: LoadingController,
    private platform: Platform,
    public alertController: AlertController,
    private afs: AngularFirestore,
    private androidPermissions: AndroidPermissions,
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy,
    private userProfile: UserProfileService,
    public nativeStorageCheckInOut: NativeStorage
  ) {

    var currentDate = new Date();

    var date = currentDate.getDate();
    var month = currentDate.getMonth();    //Be careful! January is 0 not 1
    var year = currentDate.getFullYear();

    this.todayDocId = date + "-" + (month + 1) + "-" + year;

    this.userProfile.updateIsActive(this.emailId, this.todayDocId).subscribe(active=>{

       if(active.payload.data()['Active']){
           this.active=true
           this.checkMessage= "Check In";
            
       }
       else{
         this.active=false
       }
    }) 


    this.locationCoords = {
      latitude: "",
      longitude: "",
      accuracy: "",
      timestamp: ""
    }
    this.timetest = Date.now();

  }

   CheckIn() {
    this.showLoader();
    var currentDate = new Date();

    var date = currentDate.getDate();
    var month = currentDate.getMonth();  //Be careful! January is 0 not 1
    var year = currentDate.getFullYear();

    this.todayDocId = date + "-" + (month + 1) + "-" + year;


    if (this.checkMessage == "Check Out") {
      this.checkMessage = "Check In";
      this.checkGPSPermissionAtCheckIn();
    }
    else {
      this.checkMessage = "Check Out";
      this.checkGPSPermissionAtCheckOut();
      // this.locationAddress='';

  
    }

  }

  updateCheckIn(lat: number, lng: number): Promise<any> {

    var LoginGeoTag: any;
    var LoginTime: any;
    var loginLocation: any;

   return new Promise((resolve, reject) => {
      this.nativeGeocoder.reverseGeocode(lat, lng)
        .then((result) => {

          this.userProfile.updateCheckedIn(this.emailId, this.todayDocId).then(data => {


            if (data.payload.exists) {


              LoginGeoTag = data.payload.data()['LoginGeoTag'];

              LoginTime = data.payload.data()['LoginTime'];

              loginLocation = data.payload.data()['LoginLocation'];

              var geo = [lat, lng]

              this.locationAddress = this.generateAddress(result[0]);
              this.afs.collection(this.employeesCollection).doc(this.emailId).collection(this.attendanceCollection).doc(this.todayDocId).update({

                Active:true,
                InOut: firestore.FieldValue.arrayUnion({
               
                  Type: 'CheckedIn',
                  Time: new Date(),
                  Location: this.locationAddress,

                }),

              })
              resolve(this.locationAddress);
              //alert(LoginTime + ", " + LoginLocation)

            }
            else {

              var geo = [lat, lng]
              //let newResult: NativeGeocoderResultModel = JSON.parse(JSON.stringify(result));
              this.locationAddress = this.generateAddress(result[0]);
              this.afs.collection(this.employeesCollection).doc(this.emailId).collection(this.attendanceCollection).doc(this.todayDocId).set({
                
               
                Active:true,

                InOut: firestore.FieldValue.arrayUnion({
                  Type: 'CheckedIn',
                  Time: new Date(),
                  Location: this.locationAddress,
                }),

                // LoginGeoTag: geo,
                // LoginTime: new Date(),
                // LoginLocation: this.locationAddress,
                // LogoutGeoTag: [],
                // LogoutTime: '',
                // LogoutLocation: ''
              })  

              resolve(
              );
            }


          })

        })
        .catch((error: any) => {
          reject(error);
        });
    });
  }

  updateCheckOut(lat: number, lng: number): Promise<any> {
    return new Promise((resolve, reject) => {

      this.nativeGeocoder.reverseGeocode(lat, lng)
        .then((result) => {

          this.locationAddress = this.generateAddress(result[0])
          var geo = [lat, lng]

           // this.locationAddress = JSON.parse(JSON.stringify(result));
          this.afs.collection(this.employeesCollection).doc(this.emailId).collection(this.attendanceCollection).doc(this.todayDocId).update({

            //messages : firestore.FieldValue.arrayUnion(add),
            Active:false,
            InOut: firestore.FieldValue.arrayUnion({
              Type: 'CheckedOut',
              Location: this.locationAddress,
              Time: new Date(),
            }),

            // LogoutGeoTag: geo,
            // LogoutTime: new Date(),
            // LogoutLocation: firestore.FieldValue.arrayUnion(add),
          })
          resolve();

        })
        .catch((error: any) => {
          reject(error);
        });
    });
  }



  
  //Check if application having GPS access permission  
  checkGPSPermissionAtCheckIn() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {
 
          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPSAtCheckIn();
        } else {
 
          //If not having permission ask for permission
          this.requestGPSPermissionAtCheckIn();
        }
      },
      err => {
        alert(err);
      }
    );
  }
 
  requestGPSPermissionAtCheckIn() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPSAtCheckIn();
            },
            error => {
              //Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }
 
  askToTurnOnGPSAtCheckIn() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        this.getLocationCoordinatesAtCheckIn()
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }
 
  // Methos to get device accurate coordinates using device GPS
  getLocationCoordinatesAtCheckIn() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.locationCoords.latitude = resp.coords.latitude;
      this.locationCoords.longitude = resp.coords.longitude;
      this.locationCoords.accuracy = resp.coords.accuracy;
      this.locationCoords.timestamp = resp.timestamp;

    // this.reverseGeocode();
     this.updateCheckIn(this.locationCoords.latitude, this.locationCoords.longitude)

    }).catch((error) => {
      alert('Error getting location' + error);
    });
  }


  
// Checked Out start here

//Check if application having GPS access permission  
checkGPSPermissionAtCheckOut() {
  this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
    result => {
      if (result.hasPermission) {

        //If having permission show 'Turn On GPS' dialogue
        this.askToTurnOnGPSAtCheckOut();
      } else {

        //If not having permission ask for permission
        this.requestGPSPermissionAtCheckOut();
      }
    },
    err => {
      alert(err);
    }
  );
}

requestGPSPermissionAtCheckOut() {
  this.locationAccuracy.canRequest().then((canRequest: boolean) => {
    if (canRequest) {
      console.log("4");
    } else {
      //Show 'GPS Permission Request' dialogue
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
        .then(
          () => {
            // call method to turn on GPS
            this.askToTurnOnGPSAtCheckOut();
          },
          error => {
            //Show alert if user click on 'No Thanks'
            alert('requestPermission Error requesting location permissions ' + error)
          }
        );
    }
  });
}

askToTurnOnGPSAtCheckOut() {
  this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
    () => {
      // When GPS Turned ON call method to get Accurate location coordinates
      this.getLocationCoordinatesAtCheckOut()
    },
    error => alert('Error requesting location permissions ' + JSON.stringify(error))
  );
}

// Methos to get device accurate coordinates using device GPS
getLocationCoordinatesAtCheckOut() {
  this.geolocation.getCurrentPosition().then((resp) => {
    this.locationCoords.latitude = resp.coords.latitude;
    this.locationCoords.longitude = resp.coords.longitude;
    this.locationCoords.accuracy = resp.coords.accuracy;
    this.locationCoords.timestamp = resp.timestamp;

  // this.reverseGeocode();
   this.updateCheckOut(this.locationCoords.latitude, this.locationCoords.longitude)

  }).catch((error) => {
    alert('Error getting location' + error);
  });
}

  // checked Out end here
  generateAddress(addressObj) {
    let obj = [];
    let address = "";
    for (let key in addressObj) {
      obj.push(addressObj[key]);
    }
    obj.reverse();
    for (let val in obj) {
      if (obj[val].length - 1)  /* here fixed house no came two times */
        address += obj[val] + ', ';
    }
    return address.slice(0, -2);
  }

  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: 'Please wait ...'
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed!');
      });
    });
    this.hideLoader();
  }

  hideLoader() {
    setTimeout(() => {
      this.loadingController.dismiss();
    }, 4000);
  }
}
