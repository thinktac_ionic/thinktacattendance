
import { Router, Data } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { LoadingController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserProfileService } from '../services/user-profile.service';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page  {
  
  user: any;
  userReady: boolean = false;
   name:string;
   location:string
   office:string
   department:string
   shift:string
   type:string
  constructor(
    private googlePlus: GooglePlus,
    private nativeStorage: NativeStorage,
    public loadingController: LoadingController,
    private router: Router,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private userProfile:UserProfileService

  ) { 

  }
  ngOnInit(){
    
  this.userProfile.getUserProfile('manzoor@thinktac.com').subscribe(data => {

      
        this.name = data.payload.data()['Name'];

       this.department= data.payload.data()['Department'];

        this.location = data.payload.data()['Location'],
        this.office = data.payload.data()['Office'],
        this.type =data.payload.data()['Type'],
        this.shift = data.payload.data()['Shift']

     

  });
}

  // async ngOnInit() {
    
  //   const loading = await this.loadingController.create({
  //     message: 'Please wait...'
  //   });
  //    await loading.present();
  //    this.nativeStorage.getItem('google_user')
  //   .then(data => {
  //     this.user = {
  //       name: data.name,
  //       email: data.email,
  //       picture: data.picture,
  //     };
  //     this.userReady = true;
  //     loading.dismiss();
  //   }, error =>{
  //     console.log(error);
  //     loading.dismiss();
  //   });
  // }

  // doGoogleLogout(){
  //   this.afAuth.auth.signOut();
  //   this.googlePlus.logout()
  //   .then(res => {
  //     //user logged out so we will remove him from the NativeStorage
  //     this.nativeStorage.remove('google_user');
  //     this.router.navigate(["/login"]);
  //   }, err => {
  //     console.log(err);
  //   });
  // }

}
