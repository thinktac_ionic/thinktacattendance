import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {

  constructor(private Afs: AngularFirestore) { }

  getUserProfile(userEmail) {
    return this.Afs.collection('Employees').doc(userEmail).snapshotChanges();
  }

  updateCheckedOut(userEmail) {
    return this.Afs.collection('Employees').doc(userEmail).collection('Attendance').snapshotChanges();
  }

  
  
  updateIsActive(userEmail,todayDocId) {
    return this.Afs.collection('Employees').doc(userEmail).collection('Attendance').doc(todayDocId).snapshotChanges();
  }
  updateCheckedIn(userEmail, todayDocId ) {
    //return this.Afs.collection('Employees').doc(userEmail).collection('Attendance').doc(todayDocId).snapshotChanges();

    return new Promise<any>((resolve, reject) => {
        this.Afs.collection('Employees').doc(userEmail).collection('Attendance').doc(todayDocId).snapshotChanges()
      .subscribe(snapshots => {
        resolve(snapshots),
        err => reject(err)
       })
    });
  }

}
