// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  googleWebClientId: "676685055832-83g63in8skmt5gpqmba8i6qd9lgt0d4f.apps.googleusercontent.com",

  config :{
    apiKey: "AIzaSyC9Yfs0qQT_ZGZUNh_DpeEcRmuv_Q56uZE",
    authDomain: "thinktac-attendance-app.firebaseapp.com",
    databaseURL: "https://thinktac-attendance-app.firebaseio.com",
    projectId: "thinktac-attendance-app",
    storageBucket: "thinktac-attendance-app.appspot.com",
    messagingSenderId: "676685055832"
  }
};
 
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
